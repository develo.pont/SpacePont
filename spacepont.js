'use-strict';
function mod(x, m) {
	return (x%m+m)%m;
}
var spacepont_default={
	tuile_x:47, // nombre de tuiles horizontales
	tuile_y:29, // nombre de tuiles verticales
	tuile_w:256, // dimensions d'une tuile
	tuile_h:256,
	//tuile_x:23,  // Version avec des tuiles de 512x512
	//tuile_y:15,  // 345 fichiers au lieu de 1363
	//tuile_w:512, // même poids total.
	//tuile_h:512, // 24k/tuile contre 6k/tuile
	// On définit l'espace d'affichage avec des dimensions et un décalage en nombre de tuiles
	marge_x:1, // marge à gauche
	marge_y:1, // marge en haut
	// Décalage en pixel du centre de la tuile centrale par rapport au centre de la zone d'affichage
	// Compris entre -tuile_[wh]/2 et +tuile_[wh]/2, après on boucle et on change de tuile centrale.
	Xpx:0, // au début, on centre la tuile centrale
	Ypx:0,
	icone_w:80,
	icone_h:80,
	path_fond:'data/fond/tuile-%x_%y.jpg',
	path_constellations:'data/constellations/tuile-%x_%y.png',
	path_star:'data/vignettes/star%n.png',
	path_star_n:5,
	path_star_d:7, // d et n *doivent* être premiers entre eux.
	path_vignettes:'data/vignettes/%qui%n.png',
	path_videos:'data/videos/%qui%n.mp4',
	draggable_options:{
		revert:"invalid", //retourne à sa place initiale s'il n'est pas placé sur la bonne timeline
		//snap:true,
		//snapMode:"inner",
		//snapTolerance:8,
		cursor:"crosshair",
		scroll:false,
	},
	popup_template:'<header><h2>%titre</h2></header>\
<section>%texte</section>\
<aside><video preload="metadata" controls src="%video"></video></aside>\
<footer>%pied</footer>',
	starsToFind:37,
	stars:{
1:{c:"A",x:0.0160044,y:0.157407},
2:{c:"A",x:0.0336645,y:0.116667},
3:{c:"A",x:0.0325607,y:0.185185},
4:{c:"A",x:0.0408389,y:0.151852},
5:{c:"A",x:0.0513245,y:0.15463,qui:"vera",n:3},
6:{c:"A",x:0.0584989,y:0.149074,qui:"greg",n:5},
7:{c:"C",x:0.445916,y:0.0212963},
8:{c:"C",x:0.461369,y:0.025},
9:{c:"C",x:0.449227,y:0.0453704,qui:"greg",n:13},
10:{c:"C",x:0.450883,y:0.0166667,qui:"vera",n:16},
11:{c:"G",x:0.0772627,y:0.522222},
12:{c:"G",x:0.0789183,y:0.498148},
13:{c:"G",x:0.0860927,y:0.549074,qui:"greg",n:8},
14:{c:"G",x:0.0745033,y:0.511111,qui:"vera",n:17},
15:{c:"G",x:0.0910596,y:0.515741,qui:"vera",n:2},
16:{c:"F",x:0.922737,y:0.201852},
17:{c:"F",x:0.922737,y:0.225926},
18:{c:"F",x:0.9117,y:0.236111,qui:"greg",n:9},
19:{c:"F",x:0.924945,y:0.244444},
20:{c:"B",x:0.289735,y:0.392593},
21:{c:"B",x:0.29415,y:0.428704},
22:{c:"B",x:0.299669,y:0.419444},
23:{c:"B",x:0.317881,y:0.436111},
24:{c:"B",x:0.327263,y:0.459259,qui:"vera",n:11},
25:{c:"B",x:0.27649,y:0.430556,qui:"vera",n:13},
26:{c:"E",x:0.716887,y:0.206481},
27:{c:"E",x:0.735099,y:0.162963},
28:{c:"E",x:0.699779,y:0.125,qui:"greg",n:15},
29:{c:"E",x:0.721854,y:0.162037,qui:"greg",n:1},
30:{c:"E",x:0.719647,y:0.184259,qui:"vera",n:5},
31:{c:"D",x:0.542494,y:0.241667},
32:{c:"D",x:0.53532,y:0.27037},
33:{c:"D",x:0.538631,y:0.287963},
34:{c:"D",x:0.523731,y:0.294444,qui:"greg",n:10},
35:{c:"D",x:0.550221,y:0.290741},
36:{c:"D",x:0.552428,y:0.286111,qui:"vera",n:7},
37:{c:"D",x:0.548565,y:0.267593,qui:"greg",n:4},
38:{c:"J",x:0.459713,y:0.574074},
39:{c:"J",x:0.436534,y:0.603704},
40:{c:"J",x:0.464128,y:0.612963},
41:{c:"J",x:0.475717,y:0.6},
42:{c:"H",x:0.0717439,y:0.923148},
43:{c:"H",x:0.0816777,y:0.915741,qui:"greg",n:11},
44:{c:"H",x:0.0965784,y:0.937037,qui:"greg",n:12},
45:{c:"H",x:0.0954746,y:0.887037},
46:{c:"H",x:0.105408,y:0.887037,qui:"vera",n:6},
47:{c:"H",x:0.105408,y:0.875},
48:{c:"N",x:0.88521,y:0.835185,qui:"vera",n:15},
49:{c:"N",x:0.843819,y:0.886111,qui:"greg",n:20},
50:{c:"N",x:0.863687,y:0.891667},
51:{c:"M",x:0.559051,y:0.984259,qui:"greg",n:7},
52:{c:"M",x:0.575607,y:0.962037},
53:{c:"M",x:0.589956,y:0.955556,qui:"vera",n:1},
54:{c:"M",x:0.601545,y:0.932407,qui:"greg",n:3},
55:{c:"M",x:0.61755,y:0.918519,qui:"greg",n:19},
56:{c:"L",x:0.94426,y:0.564815},
57:{c:"L",x:0.949227,y:0.555556,qui:"vera",n:12},
58:{c:"L",x:0.961369,y:0.578704,qui:"vera",n:8},
59:{c:"L",x:0.962472,y:0.558333},
60:{c:"L",x:0.965784,y:0.552778},
61:{c:"L",x:0.974062,y:0.561111,qui:"greg",n:2},
62:{c:"L",x:0.98234,y:0.549074},
63:{c:"K",x:0.696468,y:0.558333,qui:"vera",n:9},
64:{c:"K",x:0.706402,y:0.576852},
65:{c:"K",x:0.713576,y:0.562963,qui:"greg",n:18},
66:{c:"K",x:0.728477,y:0.60463,qui:"greg",n:14},
67:{c:"I",x:0.31457,y:0.837963,qui:"greg",n:16},
68:{c:"I",x:0.337748,y:0.85463,qui:"vera",n:4},
69:{c:"I",x:0.357616,y:0.833333,qui:"greg",n:17},
70:{c:"I",x:0.35872,y:0.793519,qui:"vera",n:14},
71:{c:"I",x:0.365342,y:0.859259,qui:"greg",n:6},
72:{c:"I",x:0.371965,y:0.835185,qui:"vera",n:10},
73:{c:"I",x:0.387417,y:0.826852},
74:{c:"Z",x:0,y:0},
75:{c:"Z",x:0,y:0},
76:{c:"Z",x:0,y:0},
77:{c:"Z",x:0,y:0},
78:{c:"Z",x:0,y:0},
79:{c:"Z",x:0,y:0},
80:{c:"Z",x:0,y:0},
81:{c:"Z",x:0,y:0},
82:{c:"Z",x:0,y:0},
83:{c:"Z",x:0,y:0},
	}
}
jQuery.fn.extend({
	spacepont:function(x=spacepont_default){x.space=this;x.name=this.attr("id")||this.attr("id",(Math.random()+1).toString(36).substring(2,7)).attr("id");return spacepont(x)},
	/* adds or removes a class depending on a test result */
	ifClass0:function(t,c,d){d&&t?this.removeClass(d):this.addClass(d);return t?this.addClass(c):this.removeClass(c)},
	ifClass:function(t,c,d){return t?((d&&this.removeClass(d)||this).addClass(c)):(d&&this.removeClass(c).addClass(d)||this.removeClass(c))},
});

function spacepont(space) {
	function     tuile(x, y) { return space.path_fond.replace('%x', x).replace('%y', y); }
	function constuile(x, y) { return space.path_constellations.replace('%x', x).replace('%y', y); }
	function vignette_img(id) {
		return space.path_vignettes.replace('%qui', space.stars[id].qui[0]).replace('%n', space.stars[id].n); }
	function star_img(id) {
		return space.path_star.replace('%n', id*space.path_star_d%space.path_star_n+1); }
	function video(id) {
		return space.path_videos.replace('%qui', space.stars[id].qui[0]).replace('%n', space.stars[id].n); }
	function vignette_id(id) { return 'vignette_'+space.stars[id].qui[0]+space.stars[id].n; }
	function     star_id(id) { return 'star_'+id; }
	function    popup_id(id) { return 'popup_'+id; }
	function timeline_id(id, n) {
		if(n)
			return 'timeline_'+id[0]+n;
		if(space.stars[id].qui)
			return 'timeline_'+space.stars[id].qui[0]+space.stars[id].n;
		return false;
	}
	function star_left(id) {
		return mod(space.stars[id].x*space.width -zx+space.icone_w*.5, space.width) -space.icone_w; }
	function star_top(id) {
		return mod(space.stars[id].y*space.height-zy+space.icone_h*.5, space.height)-space.icone_h; }
	function star_found(starid, target) {
		if(!space.stars[starid].qui) return;
		if(!space.stars[starid].found) {
			space.stars[starid].found=true;
			space.starsFound++;
		}
		if(!target) target=$("#"+$('img[starid='+starid+']').attr('target'));
		$('img[starid='+starid+']').removeClass('star').removeClass('freestar').attr('style','').appendTo(target);
		console.log("stars left to find : "+(space.starsToFind-space.starsFound))
		if(space.starsToFind==space.starsFound) gameOver();
	}
	function star_place(starid, target) {
		if(!space.stars[starid].qui) return;
		if(!space.stars[starid].found) {
			space.stars[starid].found=true;
 			space.starsFound++;
		}
		//$(target).children('img')
		$('img[starid='+starid+']').removeClass('star').removeClass('freestar').attr('style','').appendTo(target);
		console.log("stars left to find : "+(space.starsToFind-space.starsFound))
		if(space.starsToFind==space.starsFound) gameOver();
	}
	function star_remove(starid, dontredraw) {
		if(!space.stars[starid].found) return;
		space.stars[starid].found=false;
		$('img[starid='+starid+']').addClass('star').prependTo(space.space.parent());
		if(!dontredraw) space.set_position();
		space.starsFound=Math.max(0, space.starsFound-1);
	}
	function star_random_remove() {
		for(var id in space.stars)
			if(space.stars[id].found && Math.random()<.25)
				star_remove(id, true);
		space.set_position();
	}
	function pixel_delta(e) {
		space.Xpx=space.tuile_w/2-e.clientX+e.target.getBoundingClientRect().left+$(window)['scrollLeft']();
		space.Ypx=space.tuile_h/2-e.clientY+e.target.getBoundingClientRect().top+$(window)['scrollTop']();
	}
	function new_popup(id) {
		$(document.createElement('article')).attr({id:popup_id(id),starid:id}).html(space.popup_template.replace('%titre','').replace('%texte',space.stars[id].texte?space.stars[id].texte:'').replace('%pied','').replace('%video',video(id))).appendTo(space.space);
	}
	/****
	 * Initialisation
	 */
	/* Variables */
	space.starsFound=0;
	space.draggable_options.start=starDragStart;
	//space.draggable_options.drag=starDrag;
	space.draggable_options.helper='clone';
	space.draggable_options.appendTo=space.space.parent();
	space.root="#"+space.name;
	space.timelines={};
	space.delta_x=Math.floor(Math.ceil(1920/space.tuile_w)/2)+space.marge_x; // tuiles horizontales autour de la tuile centrale
	space.delta_y=Math.floor(Math.ceil(1080/space.tuile_h)/2)+space.marge_y; // verticales
	space.w=space.delta_x*2+1; // nombre impair
	space.h=space.delta_y*2+1; // permet d'avoir une tuile au centre de l'écran
	// Position affichée au centre
	space.X=0;
	space.Y=0;
	// Dimensions totales du plateau de jeu
	space.width=space.tuile_x*space.tuile_w;
	space.height=space.tuile_y*space.tuile_h;
	// Positionnement et dimensionnement de la zone d'affichage
	space.space.width(space.w*space.tuile_w).height(space.h*space.tuile_h).css({position:'absolute', left:(-space.marge_x*space.tuile_w)+'px', top:(-space.marge_y*space.tuile_h)+'px'});
	/* Objets DOM */
	// Création des emplacements des tuiles
	for(var j=-space.delta_y; j<=space.delta_y; j++) for(var i=-space.delta_x; i<=space.delta_x; i++)
		//$(document.createElement('div')).html("<span>"+i+"/"+j+"</span>").click({i:i, j:j}, function(e){console.log(e.data.i+','+e.data.j);space.X=mod(space.X+e.data.i, space.tuile_x); space.Y=mod(space.Y+e.data.j, space.tuile_y); pixel_delta(e); space.set_position();}).appendTo(space.space);
		$(document.createElement('div')).html("<span>"+i+"/"+j+"</span>").appendTo(space.space);
	// Création des objets des vignettes
	for(var id in space.stars)
		if(space.stars[id].qui) {
			$(document.createElement('img')).attr({src:vignette_img(id), id:vignette_id(id), 'class':'star vignette canDrag popup-pop free '+space.stars[id].qui, target:timeline_id(id), starid:id, popup:popup_id(id)}).draggable(space.draggable_options).prependTo(space.space.parent());
			new_popup(id);
		}
	// Création de toutes les étoiles
	for(var id in space.stars)
		$(document.createElement('img')).attr({src:star_img(id), id:star_id(id), 'class':'star '+(space.stars[id].qui||'none')}).ifClass(space.stars[id].c=='Z', "trounoir").prependTo(space.space.parent());
	/****
	 * Méthodes
	 */
	// Positionne le plateau de jeu dans la zone d'affichage
	// Et place tous les objets au bons endroits (étoiles, vignettes)
	space.set_position=function(notuile) {
		// position du centre de la tuile centrale sur le plateau de jeu
		cx=space.tuile_w*(space.X+.5); // X tuiles et demi puisqu'on compte à partir de zéro
		cy=space.tuile_h*(space.Y+.5); // X tuiles et demi puisqu'on compte à partir de zéro
		// position du centre de la div d'affichage
		dx=space.space.width()/2;
		dy=space.space.height()/2;
		// position du centre de la zone d'affichage réelle (la page web)
		ax=space.space.parent().width()/2;
		ay=space.space.parent().height()/2;
		// décalage du background : +centre de la div -centre de la tuile centrale sur le plateau de jeu
		// décalage de la div     : +centre de la page web -centre de la div +décalage en pixels
		pos=(dx-cx)+'px '+(dy-cy)+'px';
		space.space.css({'background-position': pos, left:ax-dx+space.Xpx, top:ay-dy+space.Ypx});
		//console.log("Set position to "+space.X+"x"+space.Y+" at "+pos+", left:"+(ax-dx+space.Xpx)+", top:"+(ay-dy+space.Ypx)+", with delta of "+space.Xpx+"x"+space.Ypx+" pixels");
		if(!notuile) {
			// Affichage des bonnes images dans les divs des tuiles
			n=0;
			divs=$(space.root+' div');
			for(var j=-space.delta_y; j<=space.delta_y; j++) {
				y=mod(space.Y+j, space.tuile_y);
				for(var i=-space.delta_x; i<=space.delta_x; i++, n++) {
					x=mod(space.X+i, space.tuile_x);
					$(divs[n]).css('background-image', 'url('+constuile(x, y)+'),url('+tuile(x, y)+')');
				}
			}
		}
		// Calcul des coordonnées du coin en haut à gauche de la zone d'affichage (page web)
		// sur le plateau de jeu complet : +centre de la tuile centrale sur le plateau de jeu -centre de la page web
		zx=cx-ax-space.Xpx;
		zy=cy-ay-space.Ypx;
		//console.log("TopLeft pixel is at "+zx+"x"+zy);
		// Chaque icône est donc placée ici, sachant qu'une icône plus haut ou à gauche mais qui déborde
		// sur la zone d'affichage, doit être affichée pour qu'on voie le débordement :
		// left=mod(icone.x-zx+space.icone_w, space.width)-space.icone_w;
		// top=mod(icone.y-zy+space.icone_h, space.height)-space.icone_h;
		for(var id in space.stars) {
			$('#'+star_id(id)).css({left:star_left(id),top:star_top(id)});
			if(space.stars[id].qui && !space.stars[id].found)
				$('#'+vignette_id(id)).css({left:star_left(id),top:star_top(id)});
		}
		return space;
	}
	/* Action effectuée quand on lâche une vignette dans sa timeline */
	function dropBehavior(e, ui) {
		var target=$("#"+ui.draggable.attr('target'));
		var timeline=target.parent(".timeline-container");
		var targetPosition=target.offset();
		var timelineWidth=timeline.width();
		var diff=0;
		// Center of timeline is placed at center of target
		// (new timeline.scrollLeft)+timeline.width()/2+timeline.offset().left =
		// timeline.scrollLeft()+target.offset().left+target.width()/2
		// We let animate handle the boundaries
		left=(timeline.scrollLeft()+target.offset().left+target.width()/2)-(timeline.width()/2+timeline.offset().left);
		timeline.animate({'scrollLeft': left}, 200, function(){
			ui.draggable.position({
				my:"center",at: "center",of:$(target),
				using: function(pos) {
					$(this).animate(pos, 300, "linear", function() {
						star_found(ui.draggable.attr('starid'), target);
					});
				}
			})
		})
	}
	function freeBehavior(e, ui) {
		var target=$(e.target);
		var timeline=space.timelines['free'];
		var targetPosition=target.offset();
		var timelineWidth=timeline.width();
		var diff=0;
		star_found(ui.draggable.attr('starid'), target);
		left=(timeline.scrollLeft()+target.offset().left+target.width()/2)-(timeline.width()/2+timeline.offset().left);
		timeline.animate({'scrollLeft': left}, 200)
	}
	function starDragStart(e, ui) {
		ui.helper.addClass('freestar');
	}
	/* Initialisation d'une timeline */
	space.timeline=function(container, qui, n, classes) {
		var drop=dropBehavior;
		if(qui=='free') drop=freeBehavior;
		var droppable={
			accept:'.canDrag.'+qui,
			activeClass:'drop-active',
			hoverClass:'drop-hover',
			greedy: true,
			drop:drop // redirige le drop vers la cible
		};
		if(!classes) classes='';
		container=$(container);
		var timelineContainer=$(document.createElement('div')).attr({'class':'timeline '+qui+' '+classes}).appendTo(container);
		var timeline=$(document.createElement('div')).attr({'class': ' droppable timeline-container'}).appendTo(timelineContainer).droppable(droppable);
		for(var i=1; i<=n; i++)
			$(document.createElement('div')).attr({id:timeline_id(qui, i), 'class':"canDrop"}).appendTo(timeline).droppable(droppable);
		$(document.createElement('button')).attr('class',"left").html("&#9664;").prependTo(timelineContainer).click(function(e){timeline.animate({scrollLeft:timeline.scrollLeft()-320}, 100);});
		$(document.createElement('button')).attr('class',"right").html("&#9654;").appendTo(timelineContainer).click(function(e){timeline.animate({scrollLeft:timeline.scrollLeft()+320}, 100);});
		space.timelines[qui]=timeline;
		return space;
	}
	/* Initialisation du déplacement sur la carte */
	space.deplacement=function() {
		var dragging=false;
		var startX, startY, startXpx, startYpx;
		function drag_origin(e) {
			startX=e.clientX;
			startY=e.clientY;
			startXpx=space.Xpx;
			startYpx=space.Ypx;
		}
		function drag_move(e) {
			Xpx=(e.clientX-startX)+startXpx;
			Ypx=(e.clientY-startY)+startYpx;
			nTuileX=Math.round(Xpx/space.tuile_w);
			nTuileY=Math.round(Ypx/space.tuile_h);
			space.X-=nTuileX;
			space.Y-=nTuileY;
			space.Xpx=Xpx-nTuileX*space.tuile_w;
			space.Ypx=Ypx-nTuileY*space.tuile_h;
		}
		$("#spaceboard div").draggable({
			start: function(e, ui) {
				drag_origin(e);
				// On vire les images pendant le déplacement pour alléger l'affichage
				// L'image actuellement "draggée" ne s'affiche donc pas !
				$(space.root+' div').css('background-image', '');
			},
			drag: function(e, ui) {
				if(dragging) return;
				dragging=true;
				drag_move(e);
				drag_origin(e);
				// On déplace sans remettre les images !
				space.set_position(true);
				dragging=false;
			},
			stop: function(e, ui) {
				drag_move(e);
				// On annule le drag effectué par draggable, nos div sont bien là où elles étaient
				$("#spaceboard div").css({'left':'','top':''})
				// Et enfin, ici, on remet toutes les images !
				space.set_position();
			},
			cursor: "cell",
			scroll:false
		})
		return space;
	}
	/* Initialisation et affichage des différentes pages */
	space.intro=function(obj, start) {
		if(space.intro_id && !obj) return space.popup(space.intro_id);
		space.intro_id=space.popupRegister($(obj), true);
		start && space.popup(space.intro_id);
		return space;
	}
	space.credits=function(obj, start) {
		if(space.credits_id && !obj) return space.popup(space.credits_id);
		space.credits_id=space.popupRegister($(obj), true);
		start && space.popup(space.credits_id);
		return space;
	}
	space.menu=function(obj, start) {
		if(space.menu_id && !obj) return space.popup(space.menu_id);
		space.menu_id=space.popupRegister($(obj), true);
		start && space.popup(space.menu_id);
		return space;
	}
	space.pages={};
	space.page=function(obj, start) {
		if(space.pages[obj])return space.popup(space.pages[obj]);
		popup=$(document.createElement('article')).addClass("page").html("<header>« SPACE »</header>");
		$("#"+obj).clone().attr({id:''}).appendTo($(document.createElement('section')).appendTo(popup).html('<div></div>').find('div'));
		space.pages[obj]=space.popupRegister(popup, true);
		start && space.popup(space.pages[obj]);
		return space;
	}
	function trousnoirs() {
		for(var id in space.stars)
			if(space.stars[id].c=='Z') {
				space.stars[id].x=Math.random();
				space.stars[id].y=Math.random();
			}
		$(".trounoir").off('click').on('click', function(){space.trounoir();});
	}
	/* Remet le jeu à zéro */
	space.reset=function(free) {
		for(var id in space.stars)
			star_remove(id, true);
		// Position affichée au centre initialisée au hasard à chaque partie !
		space.X=Math.floor(Math.random()*space.tuile_x);
		space.Y=Math.floor(Math.random()*space.tuile_y);
		// Remise à zéro des trous noirs
		trousnoirs();
		space.set_position();
		for(var i in space.timelines) {
			timeline=space.timelines[i].parent().parent();
			if(i=='free') {
				if(free) timeline.show();
				else timeline.hide();
			} else {
				if(free) timeline.hide();
				else timeline.show();
			}
		}
		return space;
	}
	/* Trou noir, tu perds la mémoire ! */
	space.trounoir=function(obj) {
		if(obj) {
			space.trounoir_id=space.popupRegister($(obj));
			return space;
		}
		star_random_remove();
		if(space.trounoir_id) {
			space.popup(space.trounoir_id);
			setTimeout(space.popup, 700);
		}
	}
	/* Cheat-code : victoire immédiate */
	space.win=function() {
		for(var id in space.stars)
			star_found(id);
	}
	// Initialisation du module de popup
	popup_init(space);
	$(window).on('resize', space.set_position);
	// Et on démarre !
	space.reset();
	return space;
}

function gameOver(){
	space.credits();
}

function popup_init(space) {
	var doc=$('body');
	var popupCount=0,popupActive=0, modals;
	function noclick(e){if(e)e.stopPropagation()}
	function popupEscapeClose(e){if(e.keyCode===27)popupOff()}
	function popupOn(o){
		$(document).bind('keyup',popupEscapeClose);
		doc.addClass('popup-on');
		modals.css('line-height',$(window).height()+"px");
		o.removeClass('popup-off');
		o.find('audio').each(function(){this.play()});
		o.find('.animate').each(function(){$(this).addClass('animating')});
		o.find('video').each(function(){
			this.play()
			this.addEventListener('ended',function(e) {
				playnext=false
				actualPos=false
				starId=parseInt(o.find("article")[0].getAttribute("starid"));
				qui=space.stars[starId].qui
				if(space.stars[starId].found) {
					timeline=$('.timeline.'+qui+' img');
					timeline.each(function(i, el){
						if(actualPos) {
							playnext=true
							popupOff()
							el.click()
							return false
						}
						actualPos=el.getAttribute("starid")==starId;
					})
				}
				if(!playnext) popupOff()
			},false);
		});
	}
	function popupOff(e){
		if(e)e.stopPropagation();
		doc.find(".popup-modal:not(.popup-off) video").each(function(){this.pause();});
		doc.find(".popup-modal:not(.popup-off) audio").each(function(){this.pause();});
		doc.find(".popup-modal:not(.popup-off) .animating").each(function(){$(this).removeClass('animating');});
		popupActive=0;
		$(document).unbind('keyup', popupEscapeClose);
		doc.removeClass('popup-on');
		$(".popup-modal").addClass("popup-off")
	}
	space.popupRegisterHTML=function(content){
		return space.popupRegister($(document.createElement('div')).html(content))
	}
	space.popupRegister=function(content, fullpage) {
		popupCount++;
		content.attr({popupid:popupCount}).detach().appendTo($(document.createElement('span')).addClass('popup').ifClass(fullpage,'fullpage').appendTo($(document.createElement('div')).addClass('popup-modal').addClass('popup-off').attr({id:"popup-"+popupCount}).appendTo(doc).bind('click', popupOff).bind('touchstart', popupOff)).bind('click', noclick).bind('touchstart', noclick))
		modals=$('.popup-modal')
		return popupCount;
	}
	space.popup=function(id,off) {
		off=off||id==popupActive
		popupOff()
		if(!off) {
			popupActive=id
			popupOn($(".popup-modal#popup-"+id))
		}
		return space;
	}
	doc.find(".popup-pop").each(function(i){
		//console.log([$(this), $(this).attr('popup')]);
		var pid=space.popupRegister($('#'+$(this).attr('popup')));
		$(this).click(function(){space.popup(pid)});
	});
	return space;
}
